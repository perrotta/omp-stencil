#include <math.h>
#include <stdio.h>

#include "common.h"

// supports mod of negative numbers
long int mod(long int a, long int b)
{
    long int r = a % b;
    return r < 0 ? r + b : r;
}

void compare_matrices(float *a, float *b, long int n){
  long int count = 0;
  for(int i = 0; i < n; i++){
    if (fabs(a[i] - b[i]) > EPSILON){
      // printf("%d\n", i); // DEBUG
      count++;
    }
  }
  printf("Number of non-matching elements: %ld\n", count);
}

void print_speedup(double ss, double se, double ps, double pe){
    printf("speedup: %.2lf\n", (se - ss) / (pe - ps));
}
