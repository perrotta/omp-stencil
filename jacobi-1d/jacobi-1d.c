#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#include "../common.h"

#define SIZE 1000000

void kernel_jacobi_1d(int tsteps,
		   			  long int n,
			   		  float *A,
			   		  float *B){

  #pragma omp target data map(tofrom: A[:n]) \
  						  map(to: B[:n])
  {
  	for (int t = 0; t < tsteps; t++){
	  #pragma omp target teams distribute parallel for schedule(static, 1)
	  for (int i = 1; i < n - 1; i++)
        B[i] = 0.33333 * (A[i-1] + A[i] + A[i + 1]);

	  #pragma omp target teams distribute parallel for schedule(static, 1)
      for (int j = 1; j < n - 1; j++)
        A[j] = B[j];

    }
  } // target data
}

void jacobi_1d(int tsteps,
			   long int n,
			   float *A,
			   float *B){
  int t, i, j;
  for (t = 0; t < tsteps; t++){
	// stencil
    for (i = 1; i < n - 1; i++)
      B[i] = 0.33333 * (A[i-1] + A[i] + A[i + 1]);
	// copy results
    for (j = 1; j < n - 1; j++)
      A[j] = B[j];
  }
}

// void compare_matrices(float *a, float *b, long int n){
//   long int count = 0;
//   for(int i = 0; i < n; i++){
//     if (fabs(a[i] - b[i]) > EPSILON){
//       // printf("%d\n", i); // DEBUG
//       count++;
//     }
//   }
//   printf("Number of non-matching elements: %ld\n", count);
// }

int main(int argc, char *argv[]){

	long int n = SIZE;
	int tsteps = 1000;

	// allocate and initialize arrays
	float *a, *b, *d_a, *d_b;
	a = (float*) malloc(sizeof(float) * n);
	b = (float*) malloc(sizeof(float) * n);
	d_a = (float*) malloc(sizeof(float) * n);
	d_b = (float*) malloc(sizeof(float) * n);

	for (int i = 0; i < n; i++){
		a[i] = i;
		b[i] = 0;
		d_a[i] = i;
		d_b[i] = 0;
	}

	// serial
	double s_start, s_end;
 	s_start = omp_get_wtime();
	jacobi_1d(tsteps, n, a, b);
	s_end = omp_get_wtime();

	// parallel
	double p_start, p_end;
	p_start = omp_get_wtime();
	kernel_jacobi_1d(tsteps, n, d_a, d_b);
	p_end = omp_get_wtime();

	// output
	compare_matrices(a, d_a, n);
	print_speedup(s_start, s_end, p_start, p_end);
	return 0;
}
