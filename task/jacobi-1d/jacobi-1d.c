#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#include "../../common.h"

#define SIZE 250
#define STEPS 100
#define SPACE 17 // MUST BE GREATER THAN 1 !!!

// long int mod(long int a, long int b)
// {
//     long int r = a % b;
//     return r < 0 ? r + b : r;
// }

// void compare_matrices(float *a, float *b, long int n){
//   long int count = 0;
//   for(int i = 0; i < n; i++){
//     if (fabs(a[i] - b[i]) > EPSILON){
//       // printf("%d\n", i); // DEBUG
//       count++;
//     }
//   }
//   printf("Number of non-matching elements: %ld\n", count);
// }

void print_array(float *a, int n){
	for (int i = 0; i < n; i++){
		printf("%.2f ", a[i]);
	}
	printf("\n");
}

int main(int argc, char *argv[]){

  //////////////////////// SERIAL ////////////////////////

  double s_start, s_end;
  s_start = omp_get_wtime();

	// allocate arrays
	float *a, *b;
	a = (float*) malloc(sizeof(float) * SIZE);
	b = (float*) malloc(sizeof(float) * SIZE);

	// intializing arrays w/ input
	for (long int i = 0; i < SIZE; i++){
		a[i] = i;
		b[i] = 0;
	}

	// stencil
	for (int t = 0; t < STEPS; t++){

		// calculation
		for (int i = 1; i < SIZE - 1; i++)
			b[i] = 0.33333 * (a[i-1] + a[i] + a[i + 1]);
		// copy results back
		for (int i = 1; i < SIZE - 1; i++)
			a[i] = b[i];

	}
	s_end = omp_get_wtime();

	//////////////////////// PARALLEL ////////////////////////

	printf("Starting Parallel\n");
	double p_start, p_end;
	p_start = omp_get_wtime();

	// memory allocation
	float **c = (float**) malloc(sizeof(float*) * SPACE);
	for (long int i = 0; i < SPACE; i++){
		c[i] = (float*) calloc(SIZE, sizeof(float));
	}

	// c[0] <- input
	for (long int i = 0; i < SIZE; i++)
		c[0][i] = i;

  // padding
  for (long int t = 1; t < SPACE; t++){
    c[t][0] = c[0][0];
    c[t][SIZE - 1] = c[0][SIZE - 1];
  }

	#pragma omp parallel num_threads(4) default(none) shared(c)
	{
		#pragma omp single
    {
      // stencil scheduler
      long int out_idx, in_idx;
      for (long int t = 1; t <= STEPS; t++){
        out_idx = mod(t, SPACE);
        in_idx = mod(t - 1, SPACE);
        // printf("%ld %ld\n", out_idx, in_idx); DEBUG, prints the dependences
        for (long int i = 1; i < SIZE - 1; i++){
          #pragma omp task default(none) shared(c) firstprivate(out_idx, in_idx, i) \
                           depend(in: c[in_idx][i - 1], c[in_idx][i], c[in_idx][i + 1]) \
                           depend(out: c[out_idx][i])
            c[out_idx][i] = 0.33333 * (c[in_idx][i - 1] + c[in_idx][i] + c[in_idx][i + 1]);
        }
      }
		} // single
	} // parallel

	p_end = omp_get_wtime();

	//////////////////////// OUTPUT ////////////////////////

	compare_matrices(a, c[STEPS % SPACE], SIZE);
	// print_speedup(s_start, s_end, p_start, p_end);
	printf("speedup: %.2lf\n", (s_end - s_start) / (p_end - p_start));


	for (long int i = 0; i < SPACE; i++){
    free(c[i]);
  }
	free(c);
	free(a);
	free(b);
	return 0;
}
