#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#include "../../common.h"

#define SIZE 250


// void compare_matrices(float *a, float *b, long int n){
//   long int count = 0;
//   for(int i = 0; i < n; i++){
//     if (fabs(a[i] - b[i]) > EPSILON){
//       // printf("%d\n", i); // DEBUG
//       count++;
//     }
//   }
//   printf("Number of non-matching elements: %ld\n", count);
// }

void print_array(float *a, int n){
	for (int i = 0; i < n; i++){
		printf("%.2f ", a[i]);
	}
	printf("\n");
}

int main(int argc, char *argv[]){

	long int n = SIZE;
	int steps = 1000;

	// allocate arrays
	float *a, *b;
	a = (float*) malloc(sizeof(float) * n);
	b = (float*) malloc(sizeof(float) * n);

	float **c = (float**) malloc(sizeof(float*) * (steps + 1));	// matrix[steps+1][SIZE]
	c[0] = (float*) calloc(n, sizeof(float));

	// intializing arrays w/ input
	for (int i = 0; i < n; i++){
		a[i] = i;
		b[i] = 0;
		c[0][i] = i;
	}

	// serial
	double s_start, s_end;
 	s_start = omp_get_wtime();

	for (int t = 0; t < steps; t++){
		// stencil
		for (int i = 1; i < n - 1; i++)
			b[i] = 0.33333 * (a[i-1] + a[i] + a[i + 1]);
		// copy results
		for (int i = 1; i < n - 1; i++)
			a[i] = b[i];
	}
	s_end = omp_get_wtime();

	// task parallelized
	printf("Starting Parallel\n");
	double p_start, p_end;
	p_start = omp_get_wtime();


	int no_of_packets = steps / 10;
	for (int packet = 0; packet < no_of_packets; packet++){
		#pragma omp parallel num_threads(4) default(none) shared(c, n) \
												 firstprivate(packet)
		{
		  #pragma omp single
			{
				// allocate this packet memory
				for (int i = 0; i < 10; i++){
					c[(packet * 10) + 1 + i] = (float*) malloc(sizeof(float*) * n);
				}
				// stencil
			  for (int t = 0; t < 10; t++){
					// padding
					#pragma omp task default(none) shared(c, n) firstprivate(t, packet)
					{
						c[(packet * 10) + t + 1][0] = c[(packet * 10) + t][0];
						c[(packet * 10) + t + 1][n - 1] = c[(packet * 10) + t][n - 1];
					}
					// stencil, iteration t
			    for (int i = 1; i < n - 1; i++)
					#pragma omp task default(none) shared(c) \
													 firstprivate(t, i, packet) \
													 depend(in: c[t][i - 1], c[t][i], c[t][i + 1]) \
													 depend(out: c[t+1][i])
						{
		      		c[(packet * 10) + t + 1][i] = 0.33333 * (c[(packet * 10) + t][i - 1]
						 																				 + c[(packet * 10) + t][i]
																										 + c[(packet * 10) + t][i + 1]);
						}
			  }
				// deallocate this packet memory, except for the last matrix, which is the result
				for (int i = 0; i < 10; i++){
					free(c[(packet * 10) + i]);
				}
			} // single
		} // parallel
	}

	// #pragma omp parallel num_threads(4) default(none) shared(c, steps, n)
	// {
	// 	#pragma omp single nowait
	// 	  for (int t = 0; t < steps; t++){
	// 			// padding
	// 			#pragma omp task default(none) shared(c, n) firstprivate(t)
	// 			{
	// 				c[t + 1][0] = c[t][0];
	// 				c[t + 1][n - 1] = c[t][n - 1];
	// 			}
	// 			// stencil, iteration t
	// 	    for (int i = 1; i < n - 1; i++)
	// 				#pragma omp task default(none) shared(c, n) firstprivate(t, i) \
	// 												 depend(in: c[t][i - 1], c[t][i], c[t][i + 1]) \
	// 												 depend(out: c[t+1][i])
	// 				{
	// 	      	c[t + 1][i] = 0.33333 * (c[t][i - 1] + c[t][i] + c[t][i + 1]);
	// 				}
	// 	  }
	// } // parallel
	p_end = omp_get_wtime();

	// output
	compare_matrices(a, c[steps], n);
	// print_speedup(s_start, s_end, p_start, p_end);
	printf("speedup: %.2lf\n", (s_end - s_start) / (p_end - p_start));


	free(c[steps]);
	free(c);
	free(a);
	free(b);
	return 0;
}
