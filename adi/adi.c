#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "../common.h"
#include <math.h>

#define SIZE 1000

void compare_2d_matrices(float **a, float **b, long int n){
	long int count = 0;

	for (long int i = 0; i < n; i++){
		for (long int j = 0; j < n; j++){
			if (fabs(a[i][j] - b[i][j]) > EPSILON)
				count++;
				// TODO: print debug info?
		}
	}
	printf("Number of non-matching elements: %ld\n", count);
}

void print_matrix(float **m){
	for (long int i = 0; i < SIZE; i++){
		for (long int j = 0; j < SIZE; j++)
			printf("%.2f ", m[i][j]);
		printf("\n");
	}
}


// TODO: rewrite this as a linearized matrix

void kernel_adi(int tsteps,
		int n,
		float **X,
		float **A,
		float **B)
{
  int t, i1, i2;


    #pragma omp target data map(tofrom: X)
    {
      for (t = 0; t < tsteps; t++)
      {
        #pragma omp target distribute parallel for collapse(2) schedule(static, 1)
	      for (i1 = 0; i1 < n; i1++)
          for (i2 = 1; i2 < n; i2++)
          {
            X[i1][i2] = X[i1][i2] - X[i1][i2-1] * A[i1][i2] / B[i1][i2-1];
            B[i1][i2] = B[i1][i2] - A[i1][i2] * A[i1][i2] / B[i1][i2-1];
          }
        #pragma omp target distribute parallel for collapse(2) schedule(static, 1)
        for (i1 = 0; i1 < n; i1++)
          X[i1][n-1] = X[i1][n-1] / B[i1][n-1];
        #pragma omp target distribute parallel for collapse(2) schedule(static, 1)
        for (i1 = 0; i1 < n; i1++)
          for (i2 = 0; i2 < n-2; i2++)
            X[i1][n-i2-2] = (X[i1][n-2-i2] - X[i1][n-2-i2-1] * A[i1][n-i2-3]) / B[i1][n-3-i2];
	      #pragma omp target distribute parallel for collapse(2) schedule(static, 1)
        for (i1 = 1; i1 < n; i1++)
          for (i2 = 0; i2 < n; i2++) {
            X[i1][i2] = X[i1][i2] - X[i1-1][i2] * A[i1][i2] / B[i1-1][i2];
            B[i1][i2] = B[i1][i2] - A[i1][i2] * A[i1][i2] / B[i1-1][i2];
          }
	      #pragma omp target distribute parallel for schedule(static, 1)
        for (i2 = 0; i2 < n; i2++)
          X[n-1][i2] = X[n-1][i2] / B[n-1][i2];
	      #pragma omp target distribute parallel for collapse(2) schedule(static, 1)
        for (i1 = 0; i1 < n-2; i1++)
          for (i2 = 0; i2 < n; i2++)
            X[n-2-i1][i2] = (X[n-2-i1][i2] - X[n-i1-3][i2] * A[n-3-i1][i2]) / B[n-2-i1][i2];
	    }
    }
  }
}


void adi(int t_steps,
		 long int n,
		 float **X,
		 float **A,
		 float **B){

  int t, i1, i2;
  for (t = 0; t < t_steps; t++){

	  for (i1 = 0; i1 < n; i1++)
			for (i2 = 1; i2 < n; i2++){
			  X[i1][i2] = X[i1][i2] - X[i1][i2-1] * A[i1][i2] / B[i1][i2-1];
		  	B[i1][i2] = B[i1][i2] - A[i1][i2] * A[i1][i2] / B[i1][i2-1];
			}

    for (i1 = 0; i1 < n; i1++)
      X[i1][n-1] = X[i1][n-1] / B[i1][n-1];

    for (i1 = 0; i1 < n; i1++)
      for (i2 = 0; i2 < n-2; i2++)
        X[i1][n-i2-2] = (X[i1][n-2-i2] - X[i1][n-2-i2-1] * A[i1][n-i2-3]) / B[i1][n-3-i2];

    for (i1 = 1; i1 < n; i1++)
      for (i2 = 0; i2 < n; i2++) {
        X[i1][i2] = X[i1][i2] - X[i1-1][i2] * A[i1][i2] / B[i1-1][i2];
        B[i1][i2] = B[i1][i2] - A[i1][i2] * A[i1][i2] / B[i1-1][i2];
      }


    for (i2 = 0; i2 < n; i2++)
      X[n-1][i2] = X[n-1][i2] / B[n-1][i2];

    for (i1 = 0; i1 < n-2; i1++)
      for (i2 = 0; i2 < n; i2++)
        X[n-2-i1][i2] = (X[n-2-i1][i2] - X[n-i1-3][i2] * A[n-3-i1][i2]) / B[n-2-i1][i2];

  } // t_steps for
}

int main(int argc, char *argv[]){
	long int n = SIZE;
	int tsteps = 10;

	// allocate and initialize arrays
	float **a, **b, **x, **d_a, **d_b, **d_x;
	a = (float**) malloc(sizeof(float*) * SIZE);
	b = (float**) malloc(sizeof(float*) * SIZE);
	x = (float**) malloc(sizeof(float*) * SIZE);
	d_a = (float**) malloc(sizeof(float*) * SIZE);
	d_b = (float**) malloc(sizeof(float*) * SIZE);
	d_x = (float**) malloc(sizeof(float*) * SIZE);

	for (int i = 0; i < n; i++){
		a[i] = (float*) malloc(sizeof(float) * SIZE);
		d_a[i] = (float*) malloc(sizeof(float) * SIZE);
		b[i] = (float*) malloc(sizeof(float) * SIZE);
		d_b[i] = (float*) malloc(sizeof(float) * SIZE);
		x[i] = (float*) malloc(sizeof(float) * SIZE);
		d_x[i] = (float*) malloc(sizeof(float) * SIZE);
		for (int j = 0; j < SIZE; j++){
			a[i][j] = rand() + 1;
			d_a[i][j] = a[i][j];
			b[i][j] = rand() + 1;
			d_b[i][j] = b[i][j];
			x[i][j] = rand() + 1;
			d_x[i][j] = x[i][j];
		}
	}

	// serial
	double s_start, s_end;
 	s_start = omp_get_wtime();
	adi(tsteps, n, x, a, b);
	s_end = omp_get_wtime();


	// parallel
	double p_start, p_end;
	p_start = omp_get_wtime();
	kernel_adi(tsteps, n, d_x, d_a, d_b);
	p_end = omp_get_wtime();

	// // output
	compare_2d_matrices(x, d_x, n);
	print_speedup(s_start, s_end, p_start, p_end);
	return 0;
}
